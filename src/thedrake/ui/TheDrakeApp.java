package thedrake.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import thedrake.game.Board;
import thedrake.game.BoardTile;
import thedrake.game.GameState;
import thedrake.game.PositionFactory;
import thedrake.game.StandardDrakeSetup;

public class TheDrakeApp extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
    stage.setTitle("Menu");
    stage.setScene(new Scene(root));
    stage.show();
  }

  private static GameState createSampleGameState() {
    Board board = new Board(4);
    PositionFactory positionFactory = board.positionFactory();
    board = board.withTiles(new Board.TileAt(positionFactory.pos(1, 1), BoardTile.MOUNTAIN));
    return new StandardDrakeSetup().startState(board)
        .placeFromStack(positionFactory.pos(0, 0))
        .placeFromStack(positionFactory.pos(3, 3))
        .placeFromStack(positionFactory.pos(0, 1))
        .placeFromStack(positionFactory.pos(3, 2))
        .placeFromStack(positionFactory.pos(1, 0))
        .placeFromStack(positionFactory.pos(2, 3));
  }
}
