package thedrake.ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import thedrake.game.Board;
import thedrake.game.BoardTile;
import thedrake.game.PositionFactory;
import thedrake.game.StandardDrakeSetup;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GameController implements Initializable {
  private GameView gameView;
  @FXML
  private Button buttonBack;
  @FXML
  private Parent root;
  @FXML
  private Pane gameRoot;
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    gameView = new GameView();
    gameRoot.getChildren().add(gameView);
  }

  public void onBack() throws IOException {
    switchScene("menu.fxml", "Menu");
  }

  public void onRestart() throws IOException {
    switchScene("game.fxml", "The Drake");
  }

  private void switchScene(String url, String title) throws IOException {
    Stage stage = (Stage)(root.getScene().getWindow());
    Parent newRoot = FXMLLoader.load(getClass().getResource(url));
    Scene newScene = new Scene(newRoot, stage.getScene().getWidth(), stage.getScene().getHeight());

    stage.setTitle(title);
    stage.setScene(newScene);
    stage.show();
  }
}
