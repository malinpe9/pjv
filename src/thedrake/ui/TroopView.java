package thedrake.ui;

import javafx.scene.layout.Pane;
import thedrake.game.PlayingSide;
import thedrake.game.Troop;

public class TroopView extends Pane {
  private TileBackgrounds background;
  private Troop troop;
  private PlayingSide side;
  private boolean onTop;

  public TroopView(Troop troop, PlayingSide side, boolean onTop){
    this.troop = troop;
    this.side = side;
    this.onTop = onTop;
  }
}
