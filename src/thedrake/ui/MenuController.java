package thedrake.ui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {

  @FXML
  private Button buttonPlay;
  @FXML
  private Button buttonLocalMp;
  @FXML
  private Button buttonOnlineMp;
  @FXML
  private Button buttonQuit;
  @FXML
  private Parent root;

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) { }

  public void onPlay() throws IOException {
    Stage stage = (Stage)(root.getScene().getWindow());
    Parent newRoot = FXMLLoader.load(getClass().getResource("game.fxml"));
    Scene newScene = new Scene(newRoot, stage.getScene().getWidth(), stage.getScene().getHeight());

    stage.setTitle("The Drake");
    stage.setScene(newScene);
    stage.show();
  }
  public void onLocalMp(){

  }

  public void onOnlineMp(){

  }

  public void onQuit(){
    Platform.exit();
  }

}
