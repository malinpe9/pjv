package thedrake.ui;

import javafx.scene.layout.Pane;
import thedrake.game.Board;
import thedrake.game.BoardTile;
import thedrake.game.PositionFactory;
import thedrake.game.StandardDrakeSetup;

public class GameView extends Pane {
  private TroopStackView blueStackView;
  private TroopStackView orangeStackView;
  private BoardView boardView;

  public GameView(){
    Board board = new Board(4);
    PositionFactory positionFactory = board.positionFactory();
    board = board.withTiles(new Board.TileAt(positionFactory.pos(1, 1), BoardTile.MOUNTAIN));
    boardView = new BoardView(new StandardDrakeSetup().startState(board)
        .placeFromStack(positionFactory.pos(0, 0))
        .placeFromStack(positionFactory.pos(3, 3))
        .placeFromStack(positionFactory.pos(0, 1))
        .placeFromStack(positionFactory.pos(3, 2))
        .placeFromStack(positionFactory.pos(1, 0))
        .placeFromStack(positionFactory.pos(2, 3)));


    orangeStackView = new TroopStackView();

    blueStackView = new TroopStackView();

    this.getChildren().add(orangeStackView);
    this.getChildren().add(boardView);
    this.getChildren().add(blueStackView);
  }
}
