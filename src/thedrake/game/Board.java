package thedrake.game;

import java.io.PrintWriter;

public class Board implements JSONSerializable{
  private BoardTile[][] board; // TODO tests say this sould be final, problem in withTiles
  private final int dimension;
  // Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru, kde všechny dlaždice jsou prázdné, tedy BoardTile.EMPTY
  public Board(int dimension) {
    this.dimension = dimension;
    board = new BoardTile[dimension][dimension];
    // Místo pro váš kód
    for (int j = 0; j < dimension; j++){
      for (int i = 0; i < dimension; i++){
        board[j][i] = BoardTile.EMPTY;
      }
    }
  }

  // Rozměr hrací desky
  public int dimension() { return dimension; }

  // Vrací dlaždici na zvolené pozici.
  public BoardTile at(TilePos pos) {
    return board[pos.j()][pos.i()];
  }

  // Vytváří novou hrací desku s novými dlaždicemi. Všechny ostatní dlaždice zůstávají stejné
  public Board withTiles(TileAt... ats) {
    Board outboard = new Board(dimension);

    for (int j = 0; j < dimension; j++){
      for (int i = 0; i < dimension; i++){
        outboard.board[j][i] = board[j][i];
      }
    }

    for (TileAt t: ats){
      outboard.board[t.pos.j()][t.pos.i()] = t.tile;
    }
    return outboard;
  }

  // Vytvoří instanci PositionFactory pro výrobu pozic na tomto hracím plánu
  public PositionFactory positionFactory() {
    return new PositionFactory(dimension);
  }

  public static class TileAt {
    public final BoardPos pos;
    public final BoardTile tile;

    public TileAt(BoardPos pos, BoardTile tile) {
      this.pos = pos;
      this.tile = tile;
    }
  }

  @Override
  public void toJSON(PrintWriter writer) {
    writer.printf("{\"dimension\":%d,\"tiles\":[", this.dimension);
    int count = 0;

    for(int j = 0; j < this.board.length; j++) {
      for(int i = 0; i < this.board[j].length; i++) {
        board[j][i].toJSON(writer);
        count++;
        if (count < dimension * dimension) {
          writer.printf(",");
        }
      }
    }

    writer.printf("]}");
  }
}

