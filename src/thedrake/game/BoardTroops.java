package thedrake.game;

import java.io.PrintWriter;
import java.util.*;

public class BoardTroops implements JSONSerializable{
  private final PlayingSide playingSide;
  private final Map<BoardPos, TroopTile> troopMap;
  private final TilePos leaderPosition;
  private final int guards;

  public BoardTroops(PlayingSide playingSide) {
    this(playingSide, Collections.emptyMap(), TilePos.OFF_BOARD, 0);
  }

  public BoardTroops(
      PlayingSide playingSide,
      Map<BoardPos, TroopTile> troopMap,
      TilePos leaderPosition,
      int guards) {
    this.playingSide = playingSide;
    this.troopMap = troopMap;
    this.leaderPosition = leaderPosition;
    this.guards = guards;
  }

  public Optional<TroopTile> at(TilePos pos) {
    return Optional.ofNullable(troopMap.get(pos));
  }

  public PlayingSide playingSide() { return playingSide; }

  public TilePos leaderPosition() { return leaderPosition; }

  public int guards() { return guards; }

  public boolean isLeaderPlaced() {
    return leaderPosition != TilePos.OFF_BOARD;
  }

  public boolean isPlacingGuards() {
    return guards < 2 && isLeaderPlaced();
  }

  public Set<BoardPos> troopPositions() {
    return troopMap.keySet();
  }

  public BoardTroops placeTroop(Troop troop, BoardPos target) throws IllegalArgumentException {
    // Already occupied
    if (troopPositions().contains(target)){
      throw new IllegalArgumentException("Cannot place troop on already occupied tile.");
    }

    // New Troop Map
    Map<BoardPos, TroopTile> newTroopMap = new HashMap<>(troopMap);
    newTroopMap.put(target, new TroopTile(troop, playingSide, TroopFace.AVERS));

    // New Leader Position
    // First => leader
    TilePos newLeaderPosition = isLeaderPlaced() ? leaderPosition : target;

    // New Guards
    int newGuards = isPlacingGuards() ? guards + 1 : guards;

    return new BoardTroops(playingSide, newTroopMap, newLeaderPosition, newGuards);
  }

  public BoardTroops troopStep(BoardPos origin, BoardPos target) throws IllegalArgumentException,IllegalStateException {
    if (leaderPosition == TilePos.OFF_BOARD){
      throw new IllegalStateException("Cannot move troops before leader is placed.");
    }
    if (isPlacingGuards()) {
      throw new IllegalStateException("Cannot move troops before guards are placed.");
    }
    if (!troopMap.containsKey(origin)){
      throw new IllegalArgumentException("Cannot move empty tile.");
    }
    if (troopMap.containsKey(target)){
      throw new IllegalArgumentException("Cannot move to occupied tile.");
    }

    Map<BoardPos, TroopTile> newTroopMap = new HashMap<>(troopMap);
    TroopTile troop = newTroopMap.remove(origin).flipped();
    newTroopMap.put(target, troop);

    TilePos newLeaderPosition = leaderPosition.equals(origin) ? target : leaderPosition;

    return new BoardTroops(playingSide, newTroopMap, newLeaderPosition, guards);
  }

  public BoardTroops troopFlip(BoardPos origin) {
    // Already implemented in the assignment, unchanged
    if (!isLeaderPlaced()) {
      throw new IllegalStateException(
          "Cannot move troops before the leader is placed.");
    }

    if (isPlacingGuards()) {
      throw new IllegalStateException(
          "Cannot move troops before guards are placed.");
    }

    if (!at(origin).isPresent())
      throw new IllegalArgumentException();

    Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
    TroopTile tile = newTroops.remove(origin);
    newTroops.put(origin, tile.flipped());

    return new BoardTroops(playingSide(), newTroops, leaderPosition, guards);
  }

  public BoardTroops removeTroop(BoardPos target) throws IllegalArgumentException {
    if (!isLeaderPlaced()){
      throw new IllegalStateException("Cannot move troops before leader is placed.");
    }
    if (isPlacingGuards()) {
      throw new IllegalStateException("Cannot move troops before guards are placed.");
    }
    if (!troopMap.containsKey(target)){
      throw new IllegalArgumentException("Cannot remove empty tile.");
    }

    Map<BoardPos, TroopTile> newTroopMap = new HashMap<>(troopMap);
    newTroopMap.remove(target);

    // TODO this *should* be wrong, but somehow isn't
    TilePos newLeaderPosition = leaderPosition.equals(target) ? TilePos.OFF_BOARD : leaderPosition;

    return new BoardTroops(playingSide, newTroopMap, newLeaderPosition, guards);
  }

  @Override
  public void toJSON(PrintWriter writer) {
    writer.printf("{\"side\":");
    playingSide.toJSON(writer);
    writer.printf(",\"leaderPosition\":");
    leaderPosition.toJSON(writer);
    writer.printf(",\"guards\":%s,\"troopMap\":", guards);
    troopMapToJSON(writer);
    writer.printf("}");
  }

  private void troopMapToJSON(PrintWriter writer) {
    writer.printf("{");
    int count = 0;
    ArrayList<BoardPos> sorted = new ArrayList<>(troopMap.keySet());
    sorted.sort((b1, b2) -> {
      if (b1.i() == b2.i()){
        return b1.j() - b2.j();
      }
      return b1.i() - b2.i();
    });
    for(BoardPos boardPos : sorted) {
      boardPos.toJSON(writer);
      writer.printf(":");
      troopMap.get(boardPos).toJSON(writer);
      count++;
      if (count < troopMap.size()) {
        writer.printf(",");
      }
    }

    writer.printf("}");
  }
}
